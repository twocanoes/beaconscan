//
//  TCLBeaconScanner.h
//  Bleu Broadcaster
//
//  Created by Tim Perfitt on 5/15/15.
//  Copyright (c) 2015 Twocanoes Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>


@interface TCLBeaconScanner : NSObject <CBCentralManagerDelegate>

-(void)startScanningWithBlock:(void (^)(NSDictionary *))inScanningBlock errorBlock:(void (^)(NSString *error))inErrorBlock;
-(void)stopScanning;
-(void)resumeScanning;
@end
