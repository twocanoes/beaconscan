//
//  main.m
//  beaconbop
//
//  Created by Tim Perfitt on 5/16/15.
//  Copyright (c) 2015 Twocanoes Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TCLBeaconScanner.h"

void usage(){
    const char *processName = [[[[NSProcessInfo processInfo] arguments][0] lastPathComponent] UTF8String];
    printf("\n----------------------\n");
    printf("%s\n",processName);
    printf("----------------------\n");

    printf("%s is a command line utility to browse iBeacon and Physical Beacons on OS X and print out the beacon information.\nCopyright 2015 Twocanoes Labs, LLC\n\n",processName);
    printf("Usage:\n");
    

    printf("%s -help                      this message\n",processName);
    printf("%s -scantime <time>           seconds to scan before exiting\n",processName);
    printf("%s -outputType <json|plain>   change output format\n",processName);

    printf("\n");
}
int main(int argc,   char * argv[]) {
    @autoreleasepool {
        NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];
        
        NSArray *arguments = [[NSProcessInfo processInfo] arguments];
        
        if ([arguments containsObject:@"-help"] || [arguments containsObject:@"-h"]) {
            
            usage();
            exit(0);
            
        }
        
        TCLBeaconScanner *scanner=[[TCLBeaconScanner alloc] init];
        [scanner startScanningWithBlock:^(NSDictionary *dict) {
            
            if ([ud objectForKey:@"outputType"]) {
                
                
                if ([[ud objectForKey:@"outputType"] isEqualToString:@"json"]) {
                    NSError *err;
                    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:&err];
                    NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                    
                    
                    printf("%s\n",[myString UTF8String]);
                }
            }
            else {
                
                [dict enumerateKeysAndObjectsUsingBlock:^(NSString * key, NSString *obj, BOOL *stop) {
                    printf("%s:%s ",[key UTF8String],[obj UTF8String]);
                }];
                printf("\n");
                
            }
            
            
        } errorBlock:^(NSString *error) {
            NSLog(@"%@",error);
            exit(-1);
        }
         ];
        
        int scantime=0;
        NSDate *now=[NSDate date];
        
        
        if ([ud objectForKey:@"scanTime"]) scantime=[[ud objectForKey:@"scanTime"] intValue];

        while (true) {
            if (scantime>0) {
                sleep(1);
                if (fabs([now timeIntervalSinceNow])>scantime) {
                  exit(0);
                    
                }
            }
            else {
                sleep(1);
            }
        }
    }
    return 0;
}
